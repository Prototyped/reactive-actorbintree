/**
 * Copyright (C) 2009-2013 Typesafe Inc. <http://www.typesafe.com>
 */
package actorbintree

import akka.actor._
import scala.collection.immutable.Queue

object BinaryTreeSet {

  trait Operation {
    def requester: ActorRef
    def id: Int
    def elem: Int
  }

  trait OperationReply {
    def id: Int
  }

  /** Request with identifier `id` to insert an element `elem` into the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Insert(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to check whether an element `elem` is present
    * in the tree. The actor at reference `requester` should be notified when
    * this operation is completed.
    */
  case class Contains(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request with identifier `id` to remove the element `elem` from the tree.
    * The actor at reference `requester` should be notified when this operation
    * is completed.
    */
  case class Remove(requester: ActorRef, id: Int, elem: Int) extends Operation

  /** Request to perform garbage collection*/
  case object GC

  /** Holds the answer to the Contains request with identifier `id`.
    * `result` is true if and only if the element is present in the tree.
    */
  case class ContainsResult(id: Int, result: Boolean) extends OperationReply
  
  /** Message to signal successful completion of an insert or remove operation. */
  case class OperationFinished(id: Int) extends OperationReply

}


class BinaryTreeSet extends Actor {

  import BinaryTreeNode._
  import BinaryTreeSet._

  def createRoot: ActorRef = context.actorOf(BinaryTreeNode.props(0, initiallyRemoved = true))

  // val root = createRoot

  // optional
  // val pendingQueue = Queue.empty[Operation]

  // optional
  def receive = normal(createRoot)

  // optional
  /** Accepts `Operation` and `GC` messages. */
  def normal(root: ActorRef): Receive = {
    case message: Operation => root ! `message`
    case GC =>
      val newRoot = createRoot
      root ! CopyTo(newRoot)
      context become garbageCollecting(newRoot, Queue.empty[Operation])
  }

  // optional
  /** Handles messages while garbage collection is performed.
    * `newRoot` is the root of the new binary tree where we want to copy
    * all non-removed elements into.
    */
  def garbageCollecting(newRoot: ActorRef, pendingQueue: Queue[Operation]): Receive = {
    case message: Operation =>
      context become garbageCollecting(newRoot, pendingQueue enqueue message)
    case GC =>
      () // ignore any GC messages
    case CopyFinished =>
      // Send all queued messages to the new tree root.
      pendingQueue foreach { queuedMessage =>
        newRoot ! queuedMessage
      }
      context become normal(newRoot)
  }
}

object BinaryTreeNode {
  trait Position

  case object Left extends Position
  case object Right extends Position

  case class CopyTo(treeNode: ActorRef)
  case object CopyFinished

  def props(elem: Int, initiallyRemoved: Boolean) = Props(classOf[BinaryTreeNode],  elem, initiallyRemoved)
}

class BinaryTreeNode(val elem: Int, initiallyRemoved: Boolean) extends Actor {

  import BinaryTreeNode._
  import BinaryTreeSet._

  // var subtrees = Map[Position, ActorRef]()
  // var removed = initiallyRemoved
  // var pendingCopyResults: Set[ActorRef] = Set.empty[ActorRef]

  def propagateOrAct(subtrees: Map[Position, ActorRef])(nonexistentChildOp: (Position, Operation) => Unit)
                    (position: Position, message: Operation) {
    if (subtrees contains position) {
      subtrees(position) ! message
    } else {
      nonexistentChildOp(position, message)
    }
  }

  def traverseForOperation(message: Operation, propagator: (Position, Operation) => Unit) {
    if (message.elem < elem) {
      propagator(Left, message)
    } else {
      propagator(Right, message)
    }
  }

  // optional
  def receive = normal(Map.empty[Position, ActorRef], initiallyRemoved)

  // optional
  /** Handles `Operation` messages and `CopyTo` requests. */
  def normal(subtrees: Map[Position, ActorRef], removed: Boolean): Receive = {
    case Insert(requester, id, insertElem) if insertElem == elem =>
      context become normal(subtrees, removed = false)
      requester ! OperationFinished(id)
    case message @ Insert(requester, id, insertElem) =>
      traverseForOperation(message, propagateOrAct(subtrees) { (position: Position, nonexistentMessage: Operation) =>
        val newNode = context.actorOf(props(nonexistentMessage.elem, initiallyRemoved = false))
        context become normal(subtrees + (position -> newNode), removed)
        nonexistentMessage.requester ! OperationFinished(nonexistentMessage.id)
      })
    case Contains(requester, id, containsElem) if containsElem == elem =>
      requester ! ContainsResult(id, result = !removed)
    case message @ Contains(requester, id, containsElem) =>
      traverseForOperation(message, propagateOrAct(subtrees) { (position: Position, nonexistentMessage: Operation) =>
        nonexistentMessage.requester ! ContainsResult(nonexistentMessage.id, result = false)
      })
    case Remove(requester, id, removeElem) if removeElem == elem =>
      context become normal(subtrees, removed = true)
      requester ! OperationFinished(id)
    case message @ Remove(requester, id, removeElem) =>
      traverseForOperation(message, propagateOrAct(subtrees) { (position: Position, nonexistentMessage: Operation) =>
        nonexistentMessage.requester ! OperationFinished(nonexistentMessage.id)
      })
    case CopyTo(otherRoot: ActorRef) =>
      // Even though there's no assurance of ordering, this happens during GC
      // during which time removes are all queued up. So the ordering is in theory not relevant
      // though it will affect the quality of the tree.

      if (!removed) {
        otherRoot ! Insert(self, elem /* yup, elem as a self-contained identifier */, elem)
      }

      subtrees foreach(_._2 ! CopyTo(otherRoot))

      val pendingCopyResults = subtrees.map(_._2).toSet

      if (!isCopied(pendingCopyResults, insertConfirmed = removed)) {
        context become copying(pendingCopyResults, insertConfirmed = removed /* "confirmed" if it's nonexistent */)
      }
    case PoisonPill =>
      shutDown(subtrees.map(_._2))
  }

  // optional
  /** `expected` is the set of ActorRefs whose replies we are waiting for,
    * `insertConfirmed` tracks whether the copy of this node to the new tree has been confirmed.
    */
  def copying(expected: Set[ActorRef], insertConfirmed: Boolean): Receive = {
    case message: Operation => throw new IllegalStateException(s"got unexpected message $message during copy")

    case OperationFinished(id) =>
      if (!isCopied(expected, insertConfirmed = true)) {
        context become copying(expected, insertConfirmed = true)
      }
    case CopyFinished =>
      val newExpected = expected - sender
      if (!isCopied(newExpected, insertConfirmed)) {
        context become copying(newExpected, insertConfirmed)
      }
    case PoisonPill =>
      shutDown(expected)
  }

  def isCopied(expected: Set[ActorRef], insertConfirmed: Boolean) =
    if (expected.isEmpty && insertConfirmed) {
      context.parent ! CopyFinished
      context stop self
      true
    } else false

  def shutDown(children: Iterable[ActorRef]) {
    children foreach { child: ActorRef =>
      child ! PoisonPill
    }
    context stop self
  }
}
